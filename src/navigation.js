//import libs
import expo, { LinearGradient, SecureStore, AppLoading } from 'expo';
import React from 'react';
import { TabNavigator } from 'react-navigation';
import RankNav from './screens/rankNav';
import Map from './screens/map';
import Camera from './screens/camera';
import Plantio from './screens/plantio';
import Login from './screens/login';
import Cadastro from './screens/cadastro';

import { request, GraphQLClient } from 'graphql-request';

var Navegacao = TabNavigator({
  Login: {screen: Login },
  Cadastro: {screen: Cadastro },
  Tab1: {screen: Plantio},
  Tab2: {screen:RankNav},
  Tab3: {screen:Map},
  }, {
    swipeEnabled:false,
    initialRouteName:'Tab2',
    navigationOptions:{
      tabBarVisible:false
    }
  }
);

export default class AppNav extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      user:null,
      loading:true
    }
  }
  async componentWillMount(){
    var that = this;
    await SecureStore.getItemAsync('loginToken').then(function(data){
      const client = new GraphQLClient('https://api.graph.cool/simple/v1/cj8dd65mt0h4y0124pcn0z290', {
        headers: {
          Authorization: 'Bearer '+data,
        },
      });
      var query = `
      query {
        user {
          id
          email
          hortas {
            id
            nome
            coords
            hortalicas{
              nome
              tipoHortalicas{
                nome
              }
            }
          }
        }
      }
      `
      client.request(query)
      .then(data => {
        that.setState({
          user:data.user,
          loading:false
        })
      })
      .catch(function(res){
        console.log(res);
      });
    });
  }
  render(){
    return(<Navegacao/>);
  }
}
