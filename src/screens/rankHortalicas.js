import expo, { AppLoading } from 'expo';
import React from 'react';
import { Text, Image, View, Alert, StyleSheet, TouchableOpacity } from 'react-native';
import styles from './../components/styles';
import {
  Container, Content, Body, Icon, Button,
  List, ListItem,
  Header, Left, Right, Title
} from 'native-base';
import Rodape from './../components/footer';

class RankHortalicas extends React.Component{
  constructor(props){
    super(props);
    this.addHortalica = this.addHortalica.bind(this);
  }
  addHortalica = function(){
    this.setState({
      adding:true
    });
  }
  addFoto = async function(){
    Expo.ImagePicker.launchCameraAsync()
    .then(function(foto){
      console.log(foto);

    });
  }
  render(){
    const { navigation } = this.props;
    const { horta } = this.props.navigation.state.params;
    var that = this;
    return (
      <Container>
        <View style={{ height:Expo.Constants.statusBarHeight }} />
        <Header style={{backgroundColor: '#1fb2a8'}}>
          <Left>
            <Button onPress={()=>navigation.goBack()} transparent>
              <Icon name={'ios-arrow-back'}/>
            </Button>
          </Left>
          <Body>
            <Title>Hortalicas</Title>
          </Body>
          <Right>
            <Button onPress={()=>this.addHortalica()}  transparent>
              <Icon name={'add'}/>
            </Button>
            <Button onPress={()=>this.addFoto()}  transparent>
              <Icon name={'camera'}/>
            </Button>
          </Right>
        </Header>
        <Content style={{padding:4}}>
          <List button>
            <ListItem style={{
                paddingLeft:0,
                marginLeft:0
              }}>
              <Text>{horta.nome}</Text>
            </ListItem>
            {horta.hortalicas.map(function(hortalicas){
              return(
                <ListItem style={{
                    paddingLeft:0,
                    marginLeft:0
                  }} key={hortalicas.id}>
                  <Text>{hortalicas.nome}</Text>
                </ListItem>
              );
            })}
          </List>
        </Content>
        <Rodape navigation={this.props.navigation}/>

      </Container>
    );
  }
}
export default RankHortalicas;
