import expo, { MapView, LinearGradient, SecureStore, AppLoading, Constants, Permissions, Location } from 'expo';
import React from 'react';
import { Text, Image, View, Alert, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import styles from './../components/styles';
import {
  Container, Content, Body, Icon, Button,
  List, ListItem,
  Header, Left, Right, Title,
  Form, Item, Input, Label, Picker
} from 'native-base';
import Rodape from './../components/footer';
import { request, GraphQLClient } from 'graphql-request';

class RankHortas extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      loading:true,
      user:false,
      adding:false,
      location:null,
      nome:'',
      coords:{},
      categoria:null
    };
    this.addHorta = this.addHorta.bind(this);
    this.saveHorta = this.saveHorta.bind(this);
    this.cancelHorta = this.cancelHorta.bind(this);
  }
  onValueChange(value: string) {
    this.setState({
      categoria: value
    });
  }
  changeState(value: string) {
    this.setState({
      nome: value
    });
  }
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permissão negada!',
      });
    }
    let location = await Location.getCurrentPositionAsync({});
    console.log('get loc', location);

    this.setState({ coords:location });
  }
  async componentWillMount(){
    this._getLocationAsync();
    var that = this;
    await SecureStore.getItemAsync('loginToken').then(function(data){
      const client = new GraphQLClient('https://api.graph.cool/simple/v1/cj8dd65mt0h4y0124pcn0z290', {
        headers: {
          Authorization: 'Bearer '+data,
        },
      });
      var query = `
      query {
        user {
          id
          email
          hortas {
            id
            nome
            coords
            hortalicas{
              id
              nome
              tipoHortalicas{
                id
                nome
              }
            }
          }
        }
      }
      `
      client.request(query)
      .then(data => {
        that.setState({
          user:data.user,
          loading:false
        })
      })
      .catch(function(res){
        console.log(res);
      });
    });
  }
  addHorta = function(){
    this.setState({
      adding:true
    });
  }
  saveHorta = async function(){
    await SecureStore.getItemAsync('loginToken').then(function(data){
      const client = new GraphQLClient('https://api.graph.cool/simple/v1/cj8dd65mt0h4y0124pcn0z290', {
        headers: {
          Authorization: 'Bearer '+data,
        },
      });
      var variables = {
        nome:this.state.nome,
        categoria:this.state.categoria,
        coords:{
          latitude:this.state.location.latitude,
          longitude:this.state.location.longitude
        },
        user:this.state.user.id
      }
      var query = `
      mutation ($nome: String!, $coords: Json, $userId: ID, $categoriaId: ID) {
        createHortas(nome: $nome, coords: $coords, userId: $userId, categoriaId: $categoriaId) {
          id
        }
      }
      `
      client.request(query,variables);
    });
    this.setState({
      adding:false
    });
  }
  cancelHorta = function(){
    this.setState({
      adding:false
    });
  }
  render(){
    if (this.state.loading) {
      return (
        <AppLoading />
      );
    }

    const { navigation } = this.props;
    const { user, location } = this.state;
    if (this.state.adding) {
      const changeState = function(e){
        console.log(e);
      }
      return(
        <Container>
          <View style={{ height:Expo.Constants.statusBarHeight }} />
          <Header style={{backgroundColor: '#1fb2a8'}}>
            <Left/>
            <Body>
              <Title>Nova Horta</Title>
            </Body>
            <Right>
              <Button onPress={()=>this.saveHorta()} transparent>
                <Text color={'white'}>Salvar</Text>
              </Button>
              <Button onPress={()=>this.cancelHorta()} transparent>
                <Text>Cancelar</Text>
              </Button>
            </Right>
          </Header>
          <Content style={{padding:4}}>
            <Form>
              <Item floatingLabel>
                <Label>Nome</Label>
                <Input value={this.state.nome} onChange={this.changeState.bind(this)} />
              </Item>
              <Picker
                mode="dropdown"
                placeholder="Categoria"
                selectedValue={this.state.categoria}
                onValueChange={this.onValueChange.bind(this)}
              >
                <Item label="Pessoal" value="cj8o3xwub1e4g0125675h7dic" />
                <Item label="Escolar" value="cj8o3tz4y1buz0124i29weyzl" />
                <Item label="Comunitária" value="cj8o3tar01buu0124ngihhkee" />
              </Picker>
            </Form>
            {location &&
              <MapView
                style={{
                  flex:1,
                  height:200
                }}
                initialRegion={{
                  latitude: location.coords.latitude,
                  longitude: location.coords.longitude,
                  latitudeDelta: 0.0800,
                  longitudeDelta: 0.0800,
                }}>
                  {listMarkers}
               </MapView>
             }
          </Content>
          <Rodape navigation={this.props.navigation}/>
        </Container>
      );
    }
    var that = this;
    return (
      <Container>
        <View style={{ height:Expo.Constants.statusBarHeight }} />
        <Header style={{backgroundColor: '#1fb2a8'}}>
          <Left/>
          <Body>
            <Title>Suas Hortas</Title>
          </Body>
          <Right>
            <Button onPress={()=>this.addHorta()} transparent>
              <Icon name={'add'}/>
            </Button>
          </Right>
        </Header>
        <Content style={{padding:4}}>
          {this.state.loading && <View><Text>Carregando...</Text></View>}
          <List button>
            {user.hortas.map(function(horta){
              return(
                <ListItem onPress={()=>navigation.navigate('Hortalicas',{horta:horta})} style={{
                    paddingLeft:0,
                    marginLeft:0
                  }} key={horta.id}>
                  <Text>{horta.nome}</Text>
                </ListItem>
              );
            })}
          </List>
        </Content>
        <Rodape navigation={this.props.navigation}/>

      </Container>
    );
  }
}
export default RankHortas;
