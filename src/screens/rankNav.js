//import libs
import Expo from 'expo';
import React from 'react';
import { StackNavigator } from 'react-navigation';
import RankHortas from './rankHortas';
import RankHortalicas from './rankHortalicas';

var RankNav = StackNavigator({
  Hortas: {screen:RankHortas},
  Hortalicas: { screen: RankHortalicas },
  }, {
    initialRouteName:'Hortas',
    headerMode:'none'
  }
);

export default RankNav;
